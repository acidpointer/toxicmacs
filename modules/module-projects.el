;;; Package --- module-projects.el

;;; Commentary:

;;; Code:

;; It's projectile
(use-package projectile
  :straight t
  :bind (("C-c p n" . projectile-discover-projects-in-directory)
         ("C-c p p" . projectile-switch-project)
         ("C-c p f" . projectile-find-file)
         ("C-c p m" . projectile-commander))
  :hook (after-init . (lambda () (projectile-mode +1))))

;; Magit - the git interface
(use-package magit
  :straight t
  :bind (("C-x g g" . magit-status)
         ("C-x g d" . magit-dispatch)
         ("C-x g R" . magit-remote-add)))

(provide 'module-projects)
;;; module-projects.el ends here
