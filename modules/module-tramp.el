;;; Package --- module-tramp.el

;;; Commentary:

;;; Code:

(use-package tramp
  :straight t
  :config
  (setq tramp-default-method "ssh")
  (when (eq window-system 'w32)
    (setq tramp-default-method "plink")))

(provide 'module-tramp)
;;; module-tramp.el ends here
