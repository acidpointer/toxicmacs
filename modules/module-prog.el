;;; Package -- module-prog

;;; Commentary:


;;; Code:

;; Completion framework
(use-package company
  :straight t
  :hook (prog-mode . company-mode)
  :config
  (setq company-minimum-prefix-length 1
        company-idle-delay 0.0))

;; Icons for company
(use-package company-box
  :straight t
  :config
  (setq company-box-scrollbar nil)
  :hook (company-mode . company-box-mode))

;; Check for errors in code on the fly
(use-package flycheck
  :straight t
  :hook (prog-mode . flycheck-mode))

;; Highlight parens.
(use-package rainbow-delimiters
  :straight t
  :hook (prog-mode . rainbow-delimiters-mode))

;; LSP support!
(use-package lsp-mode
  :straight t)


(provide 'module-prog)
;;; module-prog.el ends here
