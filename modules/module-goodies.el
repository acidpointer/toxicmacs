;;; Package --- module-goodies.el

;;; Commentary:

;;; Code:

;; Good dark theme
(use-package zenburn-theme
  :straight t
  :config
  (load-theme 'zenburn t))

;; Tells key combinations on the fly!
(use-package which-key
  :straight t
  :hook (after-init . which-key-mode))

;; Highlight cursor on buffer scroll
(use-package beacon
  :straight (auctex :host github
                    :repo "junyi-hou/beacon"
                    :files (:defaults (:exclude "*.gif" "*.org" "COPYING")))
  :hook ((prog-mode . beacon-mode)
         (text-mode . beacon-mode)))

;; Visualise undo/redo operations
(use-package undo-tree
  :straight t
  :hook ((text-mode . undo-tree-mode)
         (prog-mode . undo-tree-mode))
  :config
  (setq undo-tree-visualizer-timestamps 1)
  (setq undo-tree-visualizer-diff 1)
  (setq undo-tree-visualizer-lazy-drawing 1))

;; Nawigate windows using numbers
(use-package winum
  :straight t
  :hook (after-init . winum-mode))

(provide 'module-goodies)
;;; module-goodies.el ends here
