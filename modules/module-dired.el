;;; Package --- module-dired.el

;;; Commentary:

;;; Code:

;; Sidebar file expolorer
(use-package dired-sidebar
  :straight t
  :bind (("C-x t" . dired-sidebar-toggle-sidebar)))

(provide 'module-dired)
;;; module-dired.el ends here
