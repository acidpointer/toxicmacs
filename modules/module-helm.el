;;; Package --- module-helm.el

;;; Commentary:

;;; Code:

(use-package helm
  :straight t
  :hook ((after-init . (lambda () (helm-mode 1))))
  :config
  (progn
    (setq helm-always-two-windows nil)
    (setq helm-display-buffer-default-height 23)
    (setq helm-default-display-buffer-functions '(display-buffer-in-side-window))
    )
  :bind (("M-x" . helm-M-x)
         ("C-x C-f" . helm-find-files)
         ("C-x b" . helm-buffers-list)
         ("C-c h" . helm-mini)
         ("C-s" . helm-occur)
         (:map helm-map
               ("TAB" . helm-execute-persistent-action))
         ))

(use-package helm-descbinds
  :straight t
  :bind (("C-h b" . helm-descbinds)))

(use-package helm-describe-modes
  :straight t
  :bind (("C-h m" . helm-describe-modes)))

(use-package helm-tramp
  :straight t
  :requires tramp
  :bind (("C-c s" . helm-tramp)
         ("C-c q" . helm-tramp-quit))
  )

;; Switch (e)shells with helm!
(use-package helm-switch-shell
  :straight t
  :bind ("<f3>" . helm-switch-shell))


(provide 'module-helm)
;;; module-helm.el ends here
