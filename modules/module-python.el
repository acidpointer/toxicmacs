;;; Package -- module-python.el

;;; Commentary:

;; Requirements:
;; - python >= 3.6
;; - pip
;; - Windows 10 or Linux


;;; Code:

(defvar python-command "python3")
(if (string-equal system-type "windows-nt")
    (setq python-command "python"))

(if (executable-find python-command)
    (progn
      (use-package anaconda-mode
        :straight t
        :hook (python-mode . anaconda-mode))

      ;; LSP-server for python
      (use-package lsp-jedi
        :straight t
        :requires lsp-mode
        :config
        (add-to-list 'lsp-disabled-clients 'pyls)
        (add-to-list 'lsp-enabled-clients 'jedi)
        (setq lsp-jedi-executable-command (concat python-command " -m jedi-language-server"))
        :hook (python-mode . (lambda ()
                               (if (not (executable-find "jedi-language-server"))
                                   (message
                                    (shell-command-to-string
                                     (concat python-command " -m pip install jedi-language-server"))))
                               (require 'lsp-jedi)
                               (lsp-deferred)
                               )))

      ;; Set flycheck python executable
      (with-eval-after-load "flycheck"
        (setq flycheck-python-pycompile-executable python-command))
      )
  (message (concat "Python command '" python-command "' not found!!!")))

(provide 'module-python)
;;; module-python.el ends here
