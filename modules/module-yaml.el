;;; Package -- module-yaml.el

;;; Commentary:

;;; Code:

(use-package yaml-mode
  :straight t
  :config
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))

(provide 'module-yaml)
;;; module-yaml.el ends here
