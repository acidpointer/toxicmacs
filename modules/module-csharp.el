;;; Package --- module-csharp.el

;;; Commentary:

;;; Code:

;; Basic C# mode
(use-package csharp-mode
  :straight t)

;; Intellicense for C#
(use-package omnisharp
  :straight t
  :requires company-mode
  :config
  (add-to-list 'company-backends 'company-omnisharp)
  :hook (csharp-mode . omnisharp-mode))

(provide 'module-csharp)
;;; module-csharp.el ends here
