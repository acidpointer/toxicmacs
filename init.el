;;; Package --- init.el
;;; Commentary:

;; Author: acidpointer <acidpointer@gmail.com>

;;; Code:

;; Reduce the frequency of garbage collection by making it happen on
;; each 100MB of allocated data (the default is on every 0.76MB)
(setq gc-cons-threshold 100000000)

;; Increase amount of data which emacs reads from the process.
(setq read-process-output-max (* 1024 1024)) ; 1 mb

;; Warn when opening files bigger than 100MB
(setq large-file-warning-threshold 100000000)

;; Increase cursor blinks to 50
(setq-default blink-cursor-blinks 50)

;; Cursor blinking interval. Change if needed
(setq blink-cursor-val 0.5)
(setq-default blink-cursor-interval blink-cursor-val)
(setq-default blink-cursor-delay blink-cursor-val)

;; Disable autosave
(setq auto-save-default nil)
(setq make-backup-files nil)

(setq backup-directory-alist '(("" . "~/.emacs.d/backup")))

;; Fonts

(defun set-font-if-available (font height)
  "Set FONT and HEIGHT if available in OS."
  (if (not (null (member font (font-family-list))))
      (set-face-attribute 'default nil :height height :family font)
    (message (concat "Font '" font "' not found!"))))

(set-font-if-available "JetBrains Mono" 105)

;; Let's disable fucking toolbar!
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

;; Let's disable fucking menu bar!
(when (fboundp 'menu-bar-mode)
  (menu-bar-mode -1))

(toggle-frame-maximized)

;; Nice scrolling
(setq scroll-margin 0
      scroll-conservatively 100000
      scroll-preserve-screen-position 1)

;; Use UTF-8 everywhere
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

;; Newline at end of file
(setq require-final-newline t)

;; Tabs intpendation fix
(setq-default indent-tabs-mode nil)   ;; don't use tabs to indent
(setq-default tab-width 4)            ;; but maintain correct appearance
(setq c-basic-offset 4)               ;; I prefer 4 spaces instead 2

;; Enable line numbers globally
(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

;; Automatic parens pairing
(add-hook 'prog-mode-hook 'electric-pair-mode t)

;; Highlight parens
(add-hook 'prog-mode-hook 'show-paren-mode t)
(add-hook 'text-mode-hook 'show-paren-mode t)

;; Highlight current line
(add-hook 'prog-mode-hook 'hl-line-mode t)
(add-hook 'text-mode-hook 'hl-line-mode t)

;;; Straight:

;; Straight.el bootstrap
;; Ensure that you have git installed first.
(if (executable-find "git")
    (progn
      (defvar bootstrap-version)
      (let ((bootstrap-file
             (expand-file-name
              "straight/repos/straight.el/bootstrap.el"
              user-emacs-directory))
            (bootstrap-version 5))
        (unless (file-exists-p bootstrap-file)
          (with-current-buffer
              (url-retrieve-synchronously
               "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
               'silent 'inhibit-cookies)
            (goto-char (point-max))
            (eval-print-last-sexp)))
        (load bootstrap-file nil 'nomessage))

      (add-to-list 'load-path (concat user-emacs-directory "/modules"))
      (straight-use-package 'use-package))
  (message "GIT executable not found!"))

;;; Modules:


(with-eval-after-load 'straight
  (progn
    (require 'module-goodies)
    (require 'module-helm)
    (require 'module-dired)
    (require 'module-projects)
    (require 'module-prog)
    (require 'module-python)
    (require 'module-web)
    (require 'module-csharp)))


;;; init.el ends here
